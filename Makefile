.PHONY:

scrape:
	scrapy runspider --nolog scripts/scrape.py

send_report:
	python3 scripts/send-email-report.py