import datetime
import json
import re
from enum import Enum
from typing import Optional
from urllib import parse

import scrapy


# altura: 180
# altura hasta entrepierna: 87
# talla: M


# params = {
#     'cgid': 'outlet',
#     'prefn1': 'pc_familie',
#     'prefn2': 'pc_rahmengroesse',
#     'prefn3': 'pc_geschlecht',
#     'prefv1': 'Exceed|Exceed CFR',
#     'prefv2': 'M',
#     'prefv3': 'Unisex'
# }


class ParamTypes(Enum):
    EXCEED: str = 'exceed'
    EBYKE: str = 'e-bike'


class Params:
    def __init__(self, param_type: ParamTypes, query_params: dict, min_discount, max_price):
        self.param_type: ParamTypes = param_type
        self.query_params: dict = query_params
        self.min_discount = min_discount
        self.max_price = max_price


class ExceedParams(Params):
    def __init__(self, min_discount, max_price):
        super().__init__(ParamTypes.EXCEED, {
            'cgid': 'outlet',
            'prefn1': 'pc_familie',
            'prefn2': 'pc_rahmengroesse',
            'prefn3': 'pc_geschlecht',
            'prefv1': 'CFR|CF SLX|CF SL|Exceed|Exceed CFR',
            'prefv2': 'M',
            'prefv3': 'Unisex'
        }, min_discount, max_price)


class EbikeParams(Params):
    def __init__(self, min_discount, max_price):
        super().__init__(ParamTypes.EBYKE, {
            'cgid': 'outlet',
            'prefn1': 'pc_familie',
            'prefn2': 'pc_welt',
            'prefn3': 'pc_rahmengroesse',
            'prefn4': 'pc_geschlecht',
            'prefv1': 'Grand Canyon:ON|Spectral:ON|Neuron:ON',
            'prefv2': 'E-bike',
            'prefv3': 'M',
            'prefv4': 'Unisex'
        }, min_discount, max_price)


params_map: dict[ParamTypes, Params] = {
    ParamTypes.EXCEED: ExceedParams(200, 8000),
    ParamTypes.EBYKE: EbikeParams(200, 10000)
}


class Preferences:
    def __init__(self, url: str, params: Params):
        self.url: str = url
        self.params: Params = params

    def price_is_in_range(self, price):
        return price < self.params.max_price

    def discount_is_in_range(self, discount):
        return discount >= self.params.min_discount

    def generate_start_url(self):
        params_str = parse.urlencode(self.params.query_params, quote_via=parse.quote)
        start_url = '%s?%s' % (self.url, params_str)

        print('Generated url: ', start_url)

        return start_url


preferences_map: dict[str, Preferences] = \
    {key: Preferences('https://www.canyon.com/en-es/outlet-bikes/', params_map[key]) for key in params_map}

BASE_URL: str = 'https://www.canyon.com'


class BlogSpider(scrapy.Spider):
    name: str = 'blogspider'

    preferences_list: list[Preferences] = list(preferences_map.values())

    start_urls: list[str] = [preferences.generate_start_url() for preferences in preferences_list]

    @staticmethod
    def _extract_price(product, price_label_name: str) -> float:
        price = product.css(price_label_name).extract_first().strip()
        price = price.replace(".", "")
        price = price.replace(",", ".")
        price = re.findall(r"\d+[.]?\d*", price)
        if not price:
            price = ["0"]
        price = price[0]
        return float(price)

    @staticmethod
    def _prepare_report(product_infos: list) -> Optional[str]:
        if len(product_infos) == 0:
            return None
        html_encapsulation = """
            <html>
                <head></head>
                <body>
                    {product_reports}
                </body>
            </html>
            """
        product_reports = "".join(map(BlogSpider._prepare_product_info, product_infos))

        return html_encapsulation.format(product_reports=product_reports)

    @staticmethod
    def _prepare_product_info(product_info: list) -> str:
        bike_str = """
            <h2>{name}</h2>
            <ul>
                <li>Discount: <b>{discount}<b></li>
                <li>Price: {price_sale} ({price_original})</li>
                <li>Size:  {size}</li>
                <li>Link:  {url}</li>
            </ul>
            <br>
            """

        return bike_str.format(
            date=datetime.datetime.now(),
            name=product_info.get("name", "???"),
            size=product_info.get("dimension53", "???"),
            price_sale=product_info.get("price_sale"),
            price_original=product_info.get("price_original"),
            discount=product_info.get("discount"),
            url=product_info.get("url", "???")
        )

    @staticmethod
    def _get_product_info(product) -> dict:
        product_link = product.css('a.productTile__link ::attr(href)').extract_first()
        product_data_raw = product.css('div.productTile ::attr(data-gtm-impression)').extract_first()
        product_data = json.loads(product_data_raw)
        product_info = product_data.get("ecommerce", {}).get("impressions", [{}])[0]
        product_info["url"] = BASE_URL + product_link

        # print("PROD: " + json.dumps(product_info, indent=4))

        product_info["price_original"]: float = BlogSpider._extract_price(product,
                                                                          "div.productTile__priceOriginal::text")
        product_info["price_sale"]: float = BlogSpider._extract_price(product, "div.productTile__priceSale::text")
        product_info["discount"]: float = BlogSpider._extract_price(product, "div.productTile__priceSave::text")

        return product_info

    @staticmethod
    def _save_report(preferences: Optional[Preferences], report: Optional[str]) -> None:
        if preferences is not None:
            filename: str = 'bike-report-%s.html' % preferences.params.param_type.value
            print('Saving %s' % filename)
            with open(filename, 'w') as f:
                f.write(report)
        else:
            print('Not saving file.')
            print('Report is %s' % report)
            print('Preferences is %s' % preferences)

    @staticmethod
    def _current_preferences(response) -> Optional[Preferences]:
        if response.url not in BlogSpider.start_urls:
            print('Response url is not in BlogSpider.start_urls. '
                  'This means that there is not any matching bikes for our initial search.')
            return None

        iteration = BlogSpider.start_urls.index(response.url)
        return BlogSpider.preferences_list[iteration]

    def parse(self, response, **kwargs):
        preferences: Optional[Preferences] = BlogSpider._current_preferences(response)
        products = response.css('li.productGrid__listItem')
        product_infos: list[dict] = [BlogSpider._get_product_info(product) for product in products]

        filtered_product_infos: list[dict] = [product_info for product_info in product_infos if
                                              preferences.price_is_in_range(product_info["price_sale"]) and
                                              preferences.discount_is_in_range(product_info["discount"])]

        report: Optional[str] = BlogSpider._prepare_report(filtered_product_infos)

        BlogSpider._save_report(preferences, report)
