from __future__ import print_function

import json
from urllib import parse
from enum import Enum
from typing import Optional

import os.path
import sys
from base64 import urlsafe_b64encode
from email.mime.text import MIMEText

import requests
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

USER_CREDENTIALS_JSON_PATH = 'credentials/user-credentials.json'
USER_TOKEN_JSON_PATH = 'credentials/user-token.json'
UPDATE_GITLAB_VARS_TOKEN = 'UPDATE_GITLAB_VARS_TOKEN'

try:
    sender_email = os.environ['SENDER_EMAIL']
    receiver_users = json.loads(os.environ['RECEIVER_USERS'])
except KeyError:
    print('Please set the following mandatory environment variables: [SENDER_EMAIL, RECEIVER_USERS]')
    sys.exit(1)

try:
    if 'IS_LOCAL' in os.environ and os.environ['IS_LOCAL'] == 'true':
        is_local = True
    else:
        is_local = False
        personal_gitlab_token = os.environ[UPDATE_GITLAB_VARS_TOKEN]
        project_path = os.environ['CI_PROJECT_PATH']
except KeyError:
    print('Please set either the local or pipeline environment variables.')


# If modifying these scopes, delete the file token.json.
SCOPES = [
    'https://www.googleapis.com/auth/gmail.readonly',
    'https://www.googleapis.com/auth/gmail.modify',
    'https://www.googleapis.com/auth/gmail.compose',
    'https://www.googleapis.com/auth/gmail.send',
]


class ParamTypes(Enum):
    EXCEED: str = 'exceed'
    EBYKE: str = 'e-bike'


class User:
    def __init__(self, email: str, param_types: list[ParamTypes]):
        self.email: str = email
        self.param_types: list[ParamTypes] = param_types

    @staticmethod
    def from_string_list(param_types: list[str]) -> list[ParamTypes]:
        return [ParamTypes[param_type] for param_type in param_types]


users = [User(email, User.from_string_list(receiver_users[email])) for email in receiver_users]


def get_user_credentials():
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(USER_TOKEN_JSON_PATH):
        creds = Credentials.from_authorized_user_file(USER_TOKEN_JSON_PATH, SCOPES)

    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            print('Credentials expired, using REFRESH TOKEN.')
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(USER_CREDENTIALS_JSON_PATH, SCOPES)
            creds = flow.run_local_server(port=0)

    return creds


def update_gcp_credentials(json_credentials):
    if is_local:
        update_local_gcp_credentials(json_credentials)
    else:
        update_gitlab_gcp_credentials(json_credentials)


def update_local_gcp_credentials(json_credentials):
    with open(USER_TOKEN_JSON_PATH, 'w') as f:
        f.write(json_credentials)


def update_gitlab_gcp_credentials(json_credentials):
    project_id = parse.quote(project_path, safe='')
    key = 'GCP_TOKEN'
    data = {
        'value': json_credentials
    }
    headers = {
        'PRIVATE-TOKEN': personal_gitlab_token
    }
    r = requests.put('https://gitlab.com/api/v4/projects/%s/variables/%s' % (project_id, key),
                     data=data, headers=headers)

    r.raise_for_status()


def create_message(sender, to, subject, message_html):
    """Create a message for an email.

    Args:
      sender: Email address of the sender.
      to: Email address of the receiver.
      subject: The subject of the email message.
      message_html: The text of the email message.

    Returns:
      An object containing a base64url encoded email object.
    """
    message = MIMEText(message_html, 'html')
    message['to'] = to
    message['from'] = sender
    message['subject'] = subject
    encoded_message = urlsafe_b64encode(message.as_bytes())
    return {'raw': encoded_message.decode()}


def send_message(service, user_id, message):
    """Send an email message.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      message: Message to be sent.

    Returns:
      Sent Message.
    """
    try:
        message = (service.users().messages().send(userId=user_id, body=message)
                   .execute())
        print('Message Id: %s' % message['id'])
        return message

    except HttpError as error:
        print('An error occurred: %s' % error)


def read_file(filename) -> Optional[str]:
    try:
        with open(filename) as f:
            return f.read()
    except Exception:
        return None


def send_report(report, title, sender_email, receiver_email, credentials):
    # Moved in purpose to first line to update credentials even if there is not any email to be sent
    service = build('gmail', 'v1', credentials=credentials)

    if report is None or report == "":
        print("There are not any matches!")
        return

    message = create_message(sender_email, receiver_email, title, report)
    send_message(service, "me", message)


def send_reports(user: User, credentials):
    for param_type in user.param_types:
        filename: str = "bike-report-%s.html" % param_type.value
        print('Trying to read file: %s' % filename)
        report = read_file(filename)
        title = "Canyon bikes! - %s model" % param_type.name
        send_report(report, title, sender_email, user.email, credentials)


def main():
    credentials = get_user_credentials()
    update_gcp_credentials(credentials.to_json())

    for user in users:
        send_reports(user, credentials)


if __name__ == '__main__':
    main()
